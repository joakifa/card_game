package ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    @Test
    @DisplayName("Hand has flush")
    void hasFlush() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S',1));
        cards.add(new PlayingCard('S',2));
        cards.add(new PlayingCard('S',3));
        cards.add(new PlayingCard('S',4));
        cards.add(new PlayingCard('S',5));
        HandOfCards testHand = new HandOfCards(cards);
        assertEquals("Yes",testHand.hasFlush());
    }

    @Test
    @DisplayName("Hand does not have flush")
    void doesNotHaveFlush() {
        HandOfCards testHand = new HandOfCards(new ArrayList<>());
        assertEquals("No",testHand.hasFlush());
    }

    @Test
    @DisplayName("Cards with heart suit")
    void cardOfHearts() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H',1));
        cards.add(new PlayingCard('S',2));
        cards.add(new PlayingCard('S',3));
        cards.add(new PlayingCard('H',4));
        cards.add(new PlayingCard('S',5));
        HandOfCards testHand = new HandOfCards(cards);
        assertEquals("H1 H4",testHand.cardOfHearts());
    }

    @Test
    @DisplayName("Get sum of faces in hand")
    void getSumOfFaces() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H',1));
        cards.add(new PlayingCard('S',2));
        cards.add(new PlayingCard('S',3));
        cards.add(new PlayingCard('H',4));
        cards.add(new PlayingCard('S',5));
        HandOfCards testHand = new HandOfCards(cards);
        assertEquals(15,testHand.getSumOfFaces());
    }

    @Test
    @DisplayName("Check if queen of spades is in hand")
    void doesQueenOfSpadesExists() {
        HandOfCards testHand = new HandOfCards(new ArrayList<>());
        assertEquals("No",testHand.doesQueenOfSpadesExists());

        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S',12));
        HandOfCards handWithQueenOfSpades = new HandOfCards(cards);
        assertEquals("Yes",handWithQueenOfSpades.doesQueenOfSpadesExists());
    }

    @Test
    @DisplayName("Cards are unique")
    void areCardsUnique() {
        HandOfCards testHand = new DeckOfCards().dealHand(5);
        assertTrue(testHand.cardsAreUnique());
    }
}