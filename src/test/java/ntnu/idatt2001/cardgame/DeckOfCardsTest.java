package ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @Test
    @DisplayName("Fill deck upon instantiation")
    void fillDeck() {
        DeckOfCards testDeck = new DeckOfCards();
        assertEquals(52,testDeck.getNumberOfCards());
    }

    @Test
    void dealHand() {
        DeckOfCards testDeck = new DeckOfCards();
        HandOfCards hand = testDeck.dealHand(5);
        assertEquals(47,testDeck.getNumberOfCards());
        assertEquals(5,hand.getNumberOfCards());
    }
}