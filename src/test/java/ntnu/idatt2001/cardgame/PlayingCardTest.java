package ntnu.idatt2001.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {
    PlayingCard testCard = new PlayingCard('H',11);

    @Test
    @DisplayName("Set illegal face value")
    void setIllegalValue(){
        assertThrows(IllegalArgumentException.class, () -> new PlayingCard('H',50));
    }


    @Test
    @DisplayName("Get card as string")
    void getAsString() {
        assertEquals("H11",testCard.getAsString());
    }

    @Test
    @DisplayName("Get suit of card")
    void getSuit() {
        assertEquals('H',testCard.getSuit());
    }

    @Test
    @DisplayName("Get face value of card")
    void getFace() {
        assertEquals(11,testCard.getFace());
    }
}