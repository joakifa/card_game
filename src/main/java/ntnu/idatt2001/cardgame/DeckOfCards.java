package ntnu.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    private final char[] suit = {'S','H','D','C'};
    private ArrayList<PlayingCard> listOfCards;

    public DeckOfCards() {
        this.listOfCards = new ArrayList<>();
        fillDeck();
    }


    public void fillDeck() {
        for (char suit : suit){
            for (int i = 1; i < 14; i++){
                try {
                    listOfCards.add(new PlayingCard(suit, i));
                }catch (IllegalArgumentException e){
                    System.out.println("Face value must be between 1-13.");
                }
            }
        }
    }

    public int getNumberOfCards() {
        return listOfCards.size();
    }

    public HandOfCards dealHand(int number) {
        ArrayList<PlayingCard> randomCards = new ArrayList<>();
        Random random = new Random();
        if (getNumberOfCards() < number){
            throw new IllegalArgumentException("Not enough cards to deal that amount");
        }
        for (int i = 0; i < number; i++){
            PlayingCard card = listOfCards.get(random.nextInt(listOfCards.size()));
            randomCards.add(new PlayingCard(card.getSuit(), card.getFace()));
            listOfCards.remove(card);
        }
        return new HandOfCards(randomCards);
    }
}
