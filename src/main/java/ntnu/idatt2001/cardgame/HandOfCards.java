package ntnu.idatt2001.cardgame;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class HandOfCards {
    private ArrayList<PlayingCard> hand;

    public HandOfCards(ArrayList<PlayingCard> givenHand){
        this.hand = new ArrayList<PlayingCard>();
        hand.addAll(givenHand);
    }

    public String hasFlush(){
        if (hand.stream().filter(p -> p.getSuit() == 'S').count() >= 5 ||
                hand.stream().filter(p -> p.getSuit() == 'D').count() >= 5 ||
                hand.stream().filter(p -> p.getSuit() == 'H').count() >= 5 ||
                hand.stream().filter(p -> p.getSuit() == 'C').count() >= 5){
            return "Yes";
        }
        return "No";
    }

    public String cardOfHearts(){
       return hand.stream().filter(p -> p.getSuit() == 'H').map(PlayingCard::getAsString).
               collect(Collectors.joining(" "));
    }

    public int getSumOfFaces(){
        return hand.stream().mapToInt(PlayingCard::getFace).sum();
    }

    public String doesQueenOfSpadesExists(){
        if (hand.stream().filter(p -> p.getSuit() == 'S').anyMatch(p -> p.getFace() == 12)){
            return "Yes";
        }
        return "No";
    }

    public int getNumberOfCards(){
        return hand.size();
    }

    public ArrayList<PlayingCard> getCards(){
        return hand;
    }


    public boolean cardsAreUnique(){
        PlayingCard checkCard = hand.get(0);
        for (int i = 1; i < getNumberOfCards();i++){
            if (hand.get(i).equals(checkCard)){
                return false;
            }
        }
        return true;
    }
}
