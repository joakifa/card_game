package ntnu.idatt2001.cardgame;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class Controller {
    @FXML Button dealHand;

    @FXML Button checkHand;

    @FXML Button reset;

    @FXML Button exit;

    @FXML Button sneaky;

    @FXML Label sumOfCardsOut;

    @FXML Label cardsOfHeartsOut;

    @FXML Label flushOut;

    @FXML Label queenOfSpadesOut;

    @FXML Label deckSize;

    @FXML ImageView image1;

    @FXML ImageView image2;

    @FXML ImageView image3;

    @FXML ImageView image4;

    @FXML ImageView image5;



    private DeckOfCards deckOfCards;
    private HandOfCards handOfCards;

    @FXML
    public void initialize(){
        checkHand.setDisable(true);
        reset.setDisable(true);
        deckOfCards = new DeckOfCards();
    }

    @FXML
    private void dealHand() {
        try {
            this.handOfCards = deckOfCards.dealHand(5);
        }catch (IllegalArgumentException e){
            Alert a = new Alert(Alert.AlertType.INFORMATION);
            a.setWidth(50);
            a.setHeight(50);
            a.setHeaderText("Not enough cards");
            a.setContentText("Not enough cards to deal the given amount.");
            a.show();
        }
        checkHand.setDisable(false);
        reset.setDisable(false);
        dealHand.setDisable(true);
        deckSize.setText("Number of cards in deck:  " + deckOfCards.getNumberOfCards());
        sumOfCardsOut.setText("");
        cardsOfHeartsOut.setText("");
        flushOut.setText("");
        queenOfSpadesOut.setText("");

        Image imageURL = new Image("file:src/main/resources/images/deck.jpg");
        image1.setImage(imageURL);
        image2.setImage(imageURL);
        image3.setImage(imageURL);
        image4.setImage(imageURL);
        image5.setImage(imageURL);
    }

    @FXML
    private void checkHand() {
        checkHand.setDisable(true);
        dealHand.setDisable(false);
        sumOfCardsOut.setText(String.valueOf(handOfCards.getSumOfFaces()));
        cardsOfHeartsOut.setText(handOfCards.cardOfHearts());
        flushOut.setText(handOfCards.hasFlush());
        queenOfSpadesOut.setText(handOfCards.doesQueenOfSpadesExists());
        Image imageURL1 = new Image(handOfCards.getCards().get(0).getImageURL());
        Image imageURL2 = new Image(handOfCards.getCards().get(1).getImageURL());
        Image imageURL3 = new Image(handOfCards.getCards().get(2).getImageURL());
        Image imageURL4 = new Image(handOfCards.getCards().get(3).getImageURL());
        Image imageURL5 = new Image(handOfCards.getCards().get(4).getImageURL());
        image1.setImage(imageURL1);
        image2.setImage(imageURL2);
        image3.setImage(imageURL3);
        image4.setImage(imageURL4);
        image5.setImage(imageURL5);
    }

    @FXML
    private void reset() {
        deckOfCards = new DeckOfCards();
        deckSize.setText("Number of cards in deck:  " + deckOfCards.getNumberOfCards());
        reset.setDisable(true);
    }

    @FXML
    private void exit() {
        Stage stage = (Stage) exit.getScene().getWindow();
        stage.close();
     }

     @FXML
    private void sneaky() {
         Alert a = new Alert(Alert.AlertType.WARNING);
         a.setWidth(25);
         a.setHeight(25);
         a.setContentText("You are very sneaky");
         a.show();
     }
}